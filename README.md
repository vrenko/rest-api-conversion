## Задание 1:

### Запрос:
```sql
select u.id as 'ID', concat(u.first_name,' ', u.last_name) as 'Name', b.author as 'Author', b.name as 'Book 1', b2.name as 'Book 2' from users u left join user_books ub on u.id = ub.user_id left join books b on b.id = ub.book_id cross join books b2 where b.name!=b2.name and b.author=b2.author and u.age>6 and u.age<18 and u.id in (
select ub.user_id from user_books ub left join books b on b.id = ub.book_id where ub.user_id in (
select ub.user_id from user_books ub group by ub.user_id having count(ub.user_id)=2)
group by ub.user_id, b.author having count(*)=2)
group by  concat(u.first_name,' ', u.last_name)
```

## Задание 2

### Описание

REST API реализован на фреймворке Yii2 и предоставляет доступ к методам: 
* получения курса обмен реальных валют на биткоин, 
* обмену валюты на биткоин, 
* обмену биткоина на иную доступную валюту

### Установка
1) При настройке веб сервера указать в файле конфигурации (стандартная настройка веб-приложения Yii2):
```
<Directory "/var/www/api.loc/web">
    RewriteEngine on
    
    RewriteRule ^index.php/ - [L,R=404]
    
    RewriteCond %{REQUEST_FILENAME} !-f
    RewriteCond %{REQUEST_FILENAME} !-d
    
    RewriteRule . index.php

</Directory>
```
2) Создать и настроить базу данных.

3) В директории с приложением вызвать в терминале следующие команды, чтобы установить пакеты и добавить таблицу в базу данных:
```shell
composer install
php yii migrate
```
4) Создать нового пользователя, вызвав из терминала команду, скопировать его токен для дальнейшего использования в Authorization: Bearer <token>:
```shell
php yii token/generate
```

    Пример: NVg17YYuMStKt0qMJOOzf67fsbh4Pc7iVW8NU-aWO0mAs_oDTSfQzijJWuS99QfR


### Методы REST API

Все методы могут быть вызваны только если предоставлен корректный токен пользователя в заголовке Authorization: Bearer <token>

* Курсы обмена Биткоина на иные валюты с учетом коммиссии компании в 2%:


```url
Получить все курсы:

<your_domain>/api/v1?method=rates
```

```url
Получить курс по выбранной валюте:

<your_domain>/api/v1?method=rates&currency=<валюта>

Например:

<your_domain>/api/v1?method=rates&currency=rub
```

* Обмен валюты на биткоин с учетом комиссии 2%:


```
Обменять выбранную валюту на биткоин:

<your_domain>/api/v1?method=convert&currency_from=<валюта>&currency_to=btc&value=0.01

Пример:

<your_domain>/api/v1?method=convert&currency_from=usd&currency_to=btc&value=0.01
```

* Обмен биткоина на иную валюту с учетом комиссии 2%:

```
Обменять биткоин на выбранную валюту:

<your_domain>/api/v1?method=convert&currency_from=btc&currency_to=usd&value=0.01

Пример:

<your_domain>/api/v1?method=convert&currency_from=btc&currency_to=usd&value=0.01
```

### Дополнительные команды

Данные команды позволяют администратору сгенерировать пользователям токены:

* Сгенерировать нового пользователя с готовым токеном доступа

```shell
php yii token/generate
```

* Сгенерировать пользователю с id = user_id новый токен доступа

```shell
php yii token/generate user_id
```