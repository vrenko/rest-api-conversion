<?php


namespace app\modules\v1\controllers;


use app\filter\CustomAuthFilter;
use Throwable;
use yii\base\Action;
use yii\filters\AccessControl;
use yii\helpers\ArrayHelper;
use yii\web\BadRequestHttpException;
use yii\web\Response;
use Yii;
use yii\filters\VerbFilter;
use yii\rest\Controller;
use Symfony\Component\HttpClient\HttpClient;

/**
 * Main RESTful api class
 * Class ConverterController
 * @package app\modules\v1\controllers
 */
class ConverterController extends Controller
{

    /**
     * @param Action $action
     * @return bool
     * @throws BadRequestHttpException
     */
    public function beforeAction($action)
    {
        Yii::$app->response->format = Response::FORMAT_JSON;
        return parent::beforeAction($action);
    }

    /**
     * @return array|array[]
     */
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        $behaviors['authenticator'] = [
            'class' => CustomAuthFilter::class,
        ];
        $behaviors['access'] = [
            'class' => AccessControl::class,
            'only' => ['rates'],
            'rules' => [
                [
                    'actions' => ['rates', 'convert'],
                    'allow' => true,
                    'roles' => ['@'],
                ],
            ],
        ];
        $behaviors['verbs'] = [
            'class' => VerbFilter::class,
            'actions' => [
                'rates' => ['get'],
                'convert' => ['post']
            ],
        ];
        return $behaviors;
    }

    /**
     * Resolves requests to api methods, checks for all necessary variables
     * @param $method
     * @return array|string[]
     */
    public function actionIndex($method)
    {
        $response = [
            'code' => 403,
            'status' => 'error',
            'data' => 'Invalid token'

        ]; //If needed params for method not in query return error
        $params = Yii::$app->request->getQueryParams();
        if (array_key_exists('method', $params) && $params['method'] !== null) {
            if ($params['method'] === 'rates') {
                if (Yii::$app->request->getMethod() === 'GET') {
                    if (array_key_exists('currency', $params) && $params['currency'] !== null) {
                        return $this->actionRates($params['currency']);
                    } else {
                        return $this->actionRates();
                    }
                }
            }
            if ($params['method'] === 'convert') {
                if (Yii::$app->request->getMethod() === 'POST') {
                    if (array_key_exists('currency_from', $params) && $params['currency_from'] !== null &&
                        array_key_exists('currency_to', $params) && $params['currency_to'] !== null &&
                        array_key_exists('value', $params) && $params['value'] !== null && floatval($params['value'])) {
                        return $this->actionConvert($params["currency_from"], $params["currency_to"], $params["value"]);
                    }
                }
            }
        }
        Yii::$app->response->setStatusCode($response["code"]);
        return $response;
    }

    /**
     * Return last rates from https://blockchain.info/ticker and add 2% before sending to user
     * @param string|null $currency
     * @return array
     */
    public function actionRates(string $currency = null)
    {
        $client = HttpClient::create();
        try {
            $rates = $client->request('GET', 'https://blockchain.info/ticker')->toArray();
        } catch (Throwable $e) {
            Yii::$app->response->setStatusCode($e->getCode());
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
        $currency = strtoupper($currency);
        if (ArrayHelper::keyExists($currency, $rates, false)) {
            $currencyRates[$currency] = (float)$rates[$currency]['last'] + (float)$rates[$currency]['last'] * 0.02;
        } else {
            $currencyRates = array_combine(array_keys($rates), array_column($rates, 'last'));
            foreach ($currencyRates as $currency => $rate) {
                $currencyRates[$currency] = (float)$rate + (float)$rate * 0.02;
            }
            asort($currencyRates);
        }

        return [
            'status' => 'success',
            'code' => 200,
            'data' => $currencyRates
        ];
    }

    /**
     * Convert from chosen currency to BTC or BTC to chosen currency adding 2% of company tax
     * @param string $currency_from
     * @param string $currency_to
     * @param float $value
     * @return array|string[]
     */
    public function actionConvert(string $currency_from, string $currency_to, float $value)
    {
        $client = HttpClient::create();
        try {
            $rates = $client->request('GET', 'https://blockchain.info/ticker')->toArray();
        } catch (Throwable $e) {
            Yii::$app->response->setStatusCode($e->getCode());
            return [
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
        $response = [
            'status' => 'success',
            'code' => 200,
            'data' => [
                'currency_from' => strtoupper($currency_from),
                'currency_to' => strtoupper($currency_to),
                'value' => $value,
                'converted_value' => '',
                'rate' => ''
            ]
        ];
        if (($currency_from === 'BTC' || $currency_from === 'btc') && $value >= 0.01) {
            $rate = $rates[strtoupper($currency_to)]['last'] + $rates[strtoupper($currency_to)]['last'] * 0.02;
            $result = $value * $rate;
            $response['data']['rate'] = $rate;
            $response['data']['converted_value'] = number_format($result, 2, '.', '');

        } else {
            if (($currency_to === 'BTC' || $currency_to === 'btc') && $value >= 0.01) {
                $rate = $rates[strtoupper($currency_from)]['last'] + $rates[strtoupper($currency_from)]['last'] * 0.02;
                $result = $value / $rate;
                $response['data']['rate'] = $rate;
                $response['data']['converted_value'] = number_format($result, 10, '.', '');

            } else {
                $response = [
                    'code' => '403',
                    'stastus' => 'error',
                    'message' => 'Invalid token'
                ];
            }
        }

        Yii::$app->response->setStatusCode($response["code"]);
        return $response;
    }
}