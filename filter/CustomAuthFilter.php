<?php

namespace app\filter;

use yii\filters\auth\HttpBearerAuth;
use Yii;

class CustomAuthFilter extends HttpBearerAuth
{
    public function handleFailure($response)
    {
        Yii::$app->response->setStatusCode(403);
        return Yii::$app->response->data = [
            "status"=> "error",
            "code"=> 403,
            "message"=> "Invalid token"
        ];
    }
}