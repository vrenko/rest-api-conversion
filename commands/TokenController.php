<?php

namespace app\commands;

use yii\console\Controller;
use app\models\User;
use Yii;

/**
 * Class TokenController
 * ONLY FOR TESTING PURPOSES. Generate access token for existing user or generate new user with access token
 * @package app\commands
 */
class TokenController extends Controller
{
    /**
     * ONLY FOR TESTING PURPOSES. Generate access token for existing user or generate new user with access token
     * Usage:
     * php yii token/generate user_id - token for existing user with id = user_id
     * php yii token/generate  - token for non existing user
     * @param null $user_id
     * @throws \yii\base\Exception
     */
    public function actionGenerate($user_id=null)
    {
        if($user_id !== null){
            $user = User::findOne(['id'=>$user_id]);
        } else {
            $user = new User();
        }
        $user->setAttribute('access_token',  Yii::$app->security->generateRandomString(64));
        $user->save();
        print_r($user->getAttribute('access_token'));
    }
}
